# nsqadmin-auth

## Descripción

Este repositorio permite configurar un contenedor corriendo [nsqadmin](https://nsq.io/components/nsqadmin.html) detrás de un proxy de autenticación.

## Requerimientos

Es necesario contar con `docker` y `docker-compose` instalados en el sistema.

## Instrucciones

### Generar archivo de entorno

```bash
 $ cp env-example .env
```

### Generar clave de autenticado

`nginx` requiere que el usuario y la clave de autenticación estén encriptadas dentro de un archivo. Para generar la clave vamos a utilizar [docker-htpasswd](https://github.com/xmartlabs/docker-htpasswd). La imágen debe ejecutarse pasando como argumentos el usuario y la clave:

```bash
 $ docker run --rm -ti xmartlabs/htpasswd admin admin123 > nginx/htpasswd
```

El archivo resultante se almacena en la carpeta `nginx` para poder generar la imágen.


### Correr nginx

```bash
 $ docker-compose up --build nginx
```

## Notas

 * Los contenedores está configurados con la opción `network_mode` en `host`, por lo que funciona para cuando NSQ se ejecuta directamente sobre el host.
 * `nsqadmin` se inicia corriendo sobre el puerto 4171, mientras que `nginx` ocupa el 8190.
 * Es necesario bloquear el puerto ocupado por `nsqadmin` para rechazar conexiones remotas.
 * Si se desea cambiar el usuario / contraseña se deberá repetir los últimos 2 pasos para que los cambios tomen efecto.
